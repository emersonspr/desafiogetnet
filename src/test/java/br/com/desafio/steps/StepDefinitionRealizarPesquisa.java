package br.com.desafio.steps;

import org.openqa.selenium.JavascriptExecutor;

import br.com.desafio.po.ResultPO;
import br.com.desafio.screen.LandingScreen;
import br.com.desafio.screen.ResultScreen;
import br.com.desafio.sup.DSL;
import br.com.desafio.sup.Util;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;

public class StepDefinitionRealizarPesquisa extends DSL {
	LandingScreen landingScreen;
	ResultScreen resultScreen;

	public StepDefinitionRealizarPesquisa() {
		super();
	}

	@Dado("que desejo acessar o site")
	public void queDesejoAcessarOSite() {
		try {
			landingScreen = new LandingScreen();
			resultScreen = new ResultScreen();
			navegar("https://site.getnet.com.br/");

		} catch (Exception e) {
			Util.inException(e);
		}
	}

	@Quando("clicar no campo de busca")
	public void acionarOCampoDeBusca() {
		try {
			waitObj(landingScreen.logo);
			waitObj(landingScreen.acionarCampoBuscar);
			clicarElemento(landingScreen.acionarCampoBuscar);
		} catch (Exception e) {
			Util.inException(e);
		}
	}

	@Então("informar {string} e clico no botao pesquisar")
	public void informarValorPesquisaEClicoNoBotaoPesquisar(String itemPesquisa) {
		try {
			preencherCampo(landingScreen.inputCampoBuscar, itemPesquisa);
			clicarElemento(landingScreen.acionarBotaoProcurar);
		} catch (Exception e) {
			Util.inException(e);
		}
	}

	@Então("no resultado da busca, clico no link {string}")
	public void noResultadoDaBuscaClicoNoLinkResultado(String resultado) {
		try {
			waitObj(resultScreen.resultados);
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("window.scrollTo(0,100)");

			String getElement = "";
			int interator = 3;

			do {
				getElement = ResultPO.recuperarItemLink(driver, interator);
				if (!resultado.equals(getElement))
					interator++;
			} while (!resultado.equals(getElement));

			ResultPO.acionarLink(driver, interator - 2);
		} catch (Exception e) {
			Util.inException(e);
		}
	}

	@Então("verifico se a modal foi aberta com a mensagem {string}")
	public void verificoSeAModalFoiAbertaComAMensagemResultado(String resultado) {
		try {
			waitObj(resultScreen.getElement);
			Util.checarValores(resultado, obterTexto(resultScreen.getElement));
		} catch (Exception e) {
			Util.inException(e);
		}
	}
}
